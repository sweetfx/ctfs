# Overdrive CTF 2020

**CTF realizado por https://overdriveconference.com - https://twitter.com/OverDrive_Con durante el confinamiento.**

Este CTF recogía una amplia variedad de retos: web, crypto, pentesting a vm, entre otros.

La inscripción se realizó como equipo, bajo el nombre de **WeDontKnow**.
