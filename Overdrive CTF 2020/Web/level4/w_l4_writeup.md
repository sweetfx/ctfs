## Level 4 - HellP
>URL del reto http://94.140.114.24:8080/level4/

Cuando accedemos a la URL, solamente aparece el mensaje *level 4*. No hay ninguna pista, ni nada que nos pueda guiar un poco.

Por lo tanto, probamos a acceder el fichero *robots.txt*, y sorpresa, existe. Con el contenido *Disallow acf4*
http://94.140.114.24:8080/level4/robots.txt
>	Disallow acf4

Accedemos al nuevo directorio encontrado **/acf4**, y aparece el siguiente mensaje
> DOn't mess with me, I'm a junior developer!

Mirando en el código fuente, tenemos el siguiente trozo de código **javascript**.
```javascript
var xor_str = function(s,K) { 
    c = ''; 
    for(i=0; i<str.length; i++) { 
        for(ik=0; ik<K.length; ik++){
            c += String.fromCharCode(str[i].charCodeAt(0).toString(10) ^ key.charCodeAt(0).toString(10)); // XORing with letter 'K' 
        } 
    }
    return c;
}
var b = xor_str("acf9","acf10");
console.log(b);
```
Este código, se supone, debería de realizar la operación XOR de cada una de los caracteres del primer parámetro de la función, con la letra K.
Pero, viendo el código y el mensaje de la web, suponemos que esta función ha sido realizada por un desarrollador con poca experiencia (yo tendré menos) viendo los fallos del código.
Modificamos un poco el código
```javascript
var xor_str = function(str,K) { 
    c = ''; 
    for(i=0; i<str.length; i++) { 
        for(ik=0; ik<K.length; ik++){
            c += String.fromCharCode(str[i].charCodeAt(0).toString(10) ^ 'K'); // XORing with letter 'K' 
        } 
    }
    return c;
}
var b = xor_str("acf9","acf10");
console.log(b);
```
Y al ejecutar la función conseguimos *aaaaacccccfffff99999*

Con este resultado, empezamos a realizar peticiones al servidor, cambiando el directorio al que queremos acceder.

Ejecutamos diferentes pruebas, hasta que una da resultado
http://94.140.114.24:8080/level4/acf4/acf9/

Obtenemos la imagen bob.png  
![Bob image](https://gitlab.com/sweetfx/ctfs/-/raw/master/Overdrive%20CTF%202020/Web/level4/bob.png)

Descargamos la imagen y ejecutamos el comando *strings* al fichero. En la última línea tenemos **path=sss**

Probamos accediendo al directorio /acf4/acf9/sss en el navegador, y llegamos a una web donde nos dice que descarguemos la flag.

Añadimos a la URL **/flag.txt**, y el fichero existe.
http://94.140.114.24:8080/level4/acf4/acf9/sss/flag.txt

**flag{ce33143beddcae5ae728b2bb19c3feea}**
