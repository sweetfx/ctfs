## Level 1 - Perimetral 
>URL del reto: 94.140.114.24:8080/level1

Este reto nos presenta la siguiente web:  
![Web form](https://gitlab.com/sweetfx/ctfs/-/raw/master/Overdrive%20CTF%202020/Web/level1/webform.png)

Si inspeccionamos el código fuente de la página, hay una función llamada **checkme(s)**.
```javascript
// s = 'aaaa'
function checkme(s){
    if(...) {
        alert("wee need an email and an access key");
        return false;
    }
    else {
        var u = hexMD5("\x33\x38\x0e\xe8\xe1\x72\x64\x65\x42"+s+document.oppa.email.value);
        var pa = document.oppa.secret_key.value;

        if(u !== pa){
            alert(".......");
        }
        else{
            var p ="?p=";
        }
    }
}
```

Este código se ejecuta cuando pulsamos el botón submit, y su funcionalidad es comprobar que se han introducido los elementos del formulario, si esto es correcto, pasa a generar una variable en md5 con la concatenación de: una cadena en valor hexadecimal, el parámetro recibido y el valor del campo email del formulario. Y comparar el valor de esta variable con el valor del campo secret_key del formulario.  

Si los valores coinciden, se crea una variable p con un valor que nos da una pista con el siguiente paso a realizar **?p=**.

El siguiente paso es generar la cadena en MD5.
```javascript
hexMD5("\x33\x38\x0e\xe8\xe1\x72\x64\x65\x42"+s+document.oppa.email.value);

c77a7fccf2ac70f07f224c87829bb9c3
```
Y modificar la URL para realizar una petición con el parámetro **?p=cadenaMD5**
http://94.140.114.24:8080/level1/index.php?p=c77a7fccf2ac70f07f224c87829bb9c3

Al realizar la petición, obtenemos la flag
**flag{c267c02ee2b626a30ae0d755daee968c}**
