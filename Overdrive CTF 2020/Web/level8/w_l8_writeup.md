## Level 8 - Forza
>URL del reto: 94.140.114.24:8080/level8

Hacer una petición con *user=john*, y obtenemos una cookie *masterkey = sesame*

El código fuente de la web nos da una "pequeña" pista:
>hint: always check for juicy directories!



**stegcracker** a la imagen, y obtenemos un fichero .out con la siguiente salida:
password jpg: 12323ns

Contenido de la salida
>MEQwQTc3NkY3MjZCNzM2ODZGNzAwRDBBNzM3NDcyNjE3NzBEMEE2Rjc2NjU2RTBEMEE2RDYxNkU3NTY2NjE2Mzc0NzU3MjY1MEQwQTYzNkY2RDZENzU2RTY5NzM3NDBEMEE3MzZGNjM2OTY1NzQ3OTBEMEE2MzY4NjE2RTZFNjU2QzBEMEE3MDYxNzI3NDc5MEQwQTczNjE2NjY1MEQwQTc1NkU2MzZDNjUwRDBBNjE2NDZENjk2RTBEMEE2NjYxNzgwRDBBNzA2NTc0NjU3MjBEMEE2MzZGNkU3NDY1NkQ3MDZGNzI2MTcyNzkwRDBB

Lo pasamos por la web [CyberChef](https://gchq.github.io/CyberChef/) con la acción **Magic**. Y de todos los resultados, el más destacable es *base64 + hex*. Nos genera esta lista de elementos
```javascript   
workshop
straw
oven
manufacture
communist
society
channel
party
safe
uncle
admin
fax
peter
contemporary
```

Probamos a hacer peticiones cambiando el valor de user por cada uno de los elementos, y con **peter**, nos devuelve más información que con los demás
http://94.140.114.24:8080/level8/index.php?user=peter

La web muestra un mensaje indicando un enlace para obtener más información. La url del nuevo enlace es la siguiente:
http://94.140.114.24:8080/level8/backoffice/

Mirando el código fuente, está realizando peticiones a:
http://94.140.114.24:8080/level8/api/user/{id}

Vamos probando con diferentes ids, y conseguimos la siguiente información
```json
http://94.140.114.24:8080/level8/api/user/1
{
    "name":"admin",
    "description":"this is the default user",
    "password":"admin",
    "flaged":"false"
}
http://94.140.114.24:8080/level8/api/user/2
{
    "name":"john",
    "description":"you are a little pirate eh",
    "password":"snow",
    "flaged":"yes",
    "favorite_hash":"md5"aa
}
http://94.140.114.24:8080/level8/api/user/3
{
    "user":"manolo",
    "password":"aaAaaaAA",
    "favorite_event":"overdrive"
}
```

De esta información, hay que prestar atención a **john**, nos indica que su password es **snow** y que **md5** es su hash favorito.
Pasamos snow a md5: ```2b93fbdf27d43547bec8794054c28e00```.

Probamos a usar esta contraseña para descomprimir el fichero **flag.zip** encontrado en el directorio `/admin`. Y obtenemos el fichero **flag.txt** con la flag.
**flag{a8dececffbd3aa2e9faf0f4e07dda58d}**
