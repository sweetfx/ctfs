## Level 9 - Blade
>URL del reto: 94.140.114.24:8080/level9

Este reto nos enfrenta a un listado de URLs del estilo */stageN* siendo N un número desde 0 a 500.

Al probar algunos enlaces obtenemos **Not Found 404**, la idea es probarlos todos, pero no hacerlo a mano.
Para esta automatizar esta tarea nos creamos un pequeño script en **Python**.
```python
import requests
from bs4 import BeautifulSoup
from multiprocessing.pool import ThreadPool

URL = 'http://94.140.114.24:8080/level9'

r = requests.get(URL)

soup = BeautifulSoup(r.content, 'lxml')

links = []

for a in soup.find_all('a', href=True):
    links.append(a['href'])

def show_content(link):
    r = requests.get('{}/{}'.format(URL, link))
    if r.status_code != 404:
        print(link)
        with open('./{}'.format(link), 'wb') as f:
            f.write(r.content)

pool = ThreadPool(processes=10)
pool.map(show_content, links)
pool.close()
pool.join()
```

Después de ejecutar el script, la única URL de la que obtenemos respuesta es */stage202*. También se ha generado un fichero llamado *stage202_content*.

Si hacemos el comando file nos dice que el contenido es texto ASCII, y con un cat del fichero, podemos apreciar que es muy similar a un texto cifrado en base64.

Generamos un nuevo fichero con el siguiente comando ```base64 -d stage202_cotent > file```, y obtenemos la siguiente imagen.
![Bob Esponja image](https://gitlab.com/sweetfx/ctfs/-/raw/master/Overdrive%20CTF%202020/Web/level9/bobe.jpg "Bob Esponja")

Si ahora ejecutamos stregcracker sobre la imagen, junto con el diccionario rockyou. Obtenemos el fichero con la flag:
**flag{f9db051a51110d3bc949e940b3c68344}**
