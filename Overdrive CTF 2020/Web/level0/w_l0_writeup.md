# Retos Web
## Level 1 - Astral
> URL del reto: 94.140.114.24:8080/level0 

En primer lugar, he observado en el código fuente que tenía varias variables, la **c** y la **a**. También, que cada número de arriba corresponde con las primeras letras del abecedario.

Escribiendo en la URL la variable **a**, devolvía lo que ponía, entonces, ¿qué ocurriría si modifico la "b" simultáneamente?
Se imprimía la variable **c**, por lo que he escrito todas en la url
*http://94.140.114.24:8080/level0/index.php?a=61&b=62&c=63&d=64&e=65*

Y obtenemos la flag 
**flag{e33b65921a103b00bf95c8f906e1734b}**
