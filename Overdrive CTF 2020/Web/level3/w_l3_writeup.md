## Level 3 - EvilSunday
>URL del reto: 94.140.114.24:8080/level3

Accedemos a la web y lo primero que nos llama la atención es un enlace al fichero **hash.bin**. Lo descargamos y empezamos a inspeccionarlo con el comando *file*.
> hash.bin: PHP script, ASCII text, with very long lines

Esto nos indica que el contenido del fichero es posible que sea un script **PHP**. Realizamos un cat y obtenemos lo siguiente:
```php
// Parte del texto ha sido cambiada por ...
<?php
eval(gzuncompress(base64_decode('eJwVl7euhdoBRD/Hz6IADlmuyDlnGoucc+brfU2JKEDMXrOmvNLhn/prp2pIj/KfLN1LHP1vUeZzUf6zH9tWXv/8iyFBhAABigSts1+dVKgdVSBtnMcW/wEfP4NrVWiCTe4ytqefHGciVrwziVtMQtWILBSYEALbPY0JhXoUxogKzduD7Q3vq/...................................brw/GUOrzywFOjnUj74LgQmrAomuAtPfQBimgpULZvP6obAUziB6RA4wRcGo045r+vGX+Gf5GDsiRjkkjF2UyyvidqfKcIGgU2nwc7btQJNy//v13/ed/ei2Lhg==')));
?>
```

Accedemos a https://www.unphp.net/ y ejecutamos el código. 
Sorpresa, obtenemos el mismo código fuente que la página principal del reto, pero con un pequeño añadido, la flag:
```php
<?php
...
$p = $_GET['access'];

$flag="flag{3eb29d792013f8445b89d1c0cb051d4e}";
...
```

Flag del reto
**flag{3eb29d792013f8445b89d1c0cb051d4e}**