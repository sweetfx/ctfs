
## Level 18 - COVID
Observamos que nos dan una secuencia de ADN, con lo que se trata de un DNA RNA Cipher. Así que usamos la web https://www.dcode.fr/codons-genetic-code, para descifrarlo.
Al decodificarlo obtenemos la palabra PASTEBIN, viendo el resto del texto del reto, observamos que tenemos 7 caracteres más una interrogación, nos falta el ultimo carácter para un enlace de pastebin. 
Realizamos el siguiente script para averiguar cuál es ese último carácter y acceder al enlace correcto de pastebin.

```python
####
import string
import requests

URL = 'https://pastebin.com/rZSxZK9'
for num in range(0,10):
	r = request.get('{}{}'.format(URL,num))
	if r.status_code != 404:
		print(r.url)
		exit(0)

for letter in string.ascii_letters:
	r = request.get('{}{}'.format(URL,letter))
	if r.status_code != 404:
		print(r.url)
		exit(0)

####
```

Accedemos al enlace y nos muestra la flag que debemos introducir:
**flag{0eacef098a85ab7766671926fe78a7cf}**
