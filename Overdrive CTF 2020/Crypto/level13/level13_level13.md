## Level 13 - Level 13
El numero de la derecha del "|" utiliza un cifrado Polybius, esto es así ya que ninguna cifra es superior a 5. 
Dicho cifrado funciona por parejas de dos dígitos, separamos las parejas y las introducimos en un decodificador Polybius, para ello usamos la web https://www.dcode.fr/polybius-cipher

Y obtenemos: **MDFIVEPOLYBIAS**

Ahora cogemos y utilizamos "```echo -n polybius | md5sum```" para obtener el código md5.

Probamos a introducir la flag generada: 

**flag{da81023e73fa1aea0904753282d3cbe2}**

Y efectivamente esta es la flag correcta.
