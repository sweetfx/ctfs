## Level 1 - Penny
Primero creamos un fichero a partir de los 0 y 1 que nos dan en el reto. Utilizamos ```tr -d '\n'``` para eliminar los saltos de línea y obtener una unica línea de 0s y 1s.

Ahora debemos transformar dicho fichero en uno binario, utilizamos el siguiente comando:
```cat fichero | per -lpe '$_=pack"B*",$_' > binario```

Dentro del binario nos encontramos una cadena de texto en Base64:

> NzU2RjdBNzQ3QjM2MzM3ODc3MzA3OTM4MzU3NTc5MzUzNjdBNzYzODc4NzgzNjM0MzkzNjc4MzE3NzM1MzYzNTM5Mzk3Njc5Nzg3RDBEMEE

Al decodificarla obtenemos un texto en hexadecimal:
> 756F7A747B3633787730793835757935367A763878783634393678317735363539397679787D0D0A

Pasamos dicho texto a ASCII y obtenemos:
>  uozt{63xw0y85uy56zv8xx6496x1w56599vyx}

Esto ya es la flag que necesitamos, pero está cifrada. Para descifrarlo, utilizamos la sustitución, vemos que "**uozt**" es "**flag**", así pues, obtenemos un cifrado **atbash**, es decir, un cifrado "de espejo", el cifrado es inverso al alfabeto normal, descifrándolo obtenemos la flag:
**flag{63630c85d056b885f6496b15565996ae}**
