# Cryptanalysis
## Level 0 - Eureka

El texto que se nos proporciona esta codificado en base64, así utilizamos un decodificador de base64 online, con lo que obtenemos el siguiente texto cifrado:

> Pm ol ohk hufaopun jvumpkluaphs av zhf, ol dyval pa pu jpwoly, aoha pz, if zv johunpun aol vykly vm aol slaalyz vm aol hswohila, aoha uva h dvyk jvbsk il thkl vba.
> 
>mshn{l4832hi7947h52ij6218lkh5jk5iji9k}

Probamos a utilizar un cifrado cesar (https://www.dcode.fr/chiffre-cesar), obtenemos lo siguiente:

> If he had anything confidential to say, he wrote it in cipher, that is, by so changing the order of the letters of the alphabet, that not a word could be made out.
>
> flag{e4832ab7947a52bc6218eda5cd5bcb9d}

Flag del reto **flag{e4832ab7947a52bc6218eda5cd5bcb9d}**
