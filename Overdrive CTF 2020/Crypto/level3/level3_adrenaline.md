## Level 3 - Adrenaline
Nos descargamos el fichero que contiene un texto en hexadecimal, utilizamos la herramienta online CyberChef, le pasamos el fichero y decodificamos el hexadecimal. A partir de este , obtenemos un código que parece ser Morse, aplicamos el decodificador de morse, y obtenemos un texto binario.

Transformamos dicho texto binario, en un fichero binario con el siguiente comando:
```cat fichero | perl -lpe '$_=pack"B*",$_' > binario```

Realizamos un cat del fichero binario y vemos que aparte de "basura" tiene código Brainfuck dentro:
> ++++++++++\[>+>+++>+++++++>++++++++++<<<<-]>>>-------------------.++++.--.---.+++.>++.<+.>--.<+.+.--------.>.+.<+++++++++.----.---.++++++.----.+++.+.-----.>---.<++++++.------..>++++.<++++++.------.>----.<-.+++.-----.

Cogemos dicho código y lo compilamos online, consiguiendo lo siguiente:
> 37525f6d780de95284783b933f93b250

Esto parece un md5, así que probamos a introducirlo, y efectivamente esta es la flag:
**flag{37525f6d780de95284783b933f93b250}**
