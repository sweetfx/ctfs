## Level 17 - Sweet
Nos proporciona una cadena de caracteres formadas por los caracteres X e Y. Se me ocurre convertir las X a 0 y las Y convertirlas en 1, para generar un fichero binario.

Ahora lo que debemos hacer es pasar dicho fichero "binario" que es básicamente un fichero ASCII con los 0 y 1 en un fichero binario. Para ello usamos el siguiente comando:

> cat fichero | perl -lpe '$_=pack*B*",$_' > binario

Una vez abrimos el fichero binario obtenemos el siguiente código:
> ++++++++++\[>+>+++>+++++++>++++++++++<<<<-]>>>>+++++++++.---------.++.+++.+++++++++++++.-----------------.-.+..+++++++++++.----.-------.----.+++++++++++++++++.----.-----.+++++.-------.

Esto es un código de un lenguaje de programación esotérico llamado BrainFuck, utilizamos un compilador online para ejecutar dicho código. Al ejecutarlo obtenemos: **mdfivedeeplearning**.

Convertimos "**deeplearning**" a md5 y obtenemos nuestra flag: 
**flag{dfc77721cb9c5138d8d3311d9f038fef}**
