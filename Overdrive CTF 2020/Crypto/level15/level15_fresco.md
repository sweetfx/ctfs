## Level 15 - Fresco
Dentro del enlace proporcionado, obtenemos texto en hexadecimal, el cual pasamos a utf8 con la herramienta CyberChef.

Al utilizar la herramienta obtenemos texto en base64, con lo utilizamos el decodificador de base64 de la misma web (CyberChef), vemos que obtenemos otro texto en base64. Así pues, aplicamos decodificación de base64 20 veces hasta que finalmente obtenemos lo siguiente: **mdfiveprince**

Pasamos **prince** a md5, e introducimos la flag
**flag{2077e4a6bafa9b4e7b55e1fff16818af}**

