# Network Analysis
## Level 0 - tranXmission

Empezamos inspeccionando el fichero pcapng para ver si encontramos algo interesante. Encontramos una conexión FTP, nos centramos en los paquetes FTP.

Seguimos analizando los paquetes que utilizan el protocolo FTP, y llegamos al paquete **406**, comando **STOP sec.rar**. 
Como el tráfico no está cifrado, nos fijamos en el paquete **412**, **FTP-DATA**. 
Hacemos `Follow TCP Stream` del paquete, cambiamos la salida a formato RAW y guardamos los datos como sec.rar.

Al intentar abrir el fichero nos pide una contraseña, por lo que ahora toca romper la contraseña. 

Primero extraemos el hash: 
```$ rar2john sec.rar > key.hash```

Una vez obtenido el hash, intentamos un ataque por fuerza bruta con john y el diccionario rockyou: 
```$ john --wordlist=rockyou.txt key.hash```

Obtenemos la contraseña del fichero: **1234**.
Extraemos el contenido del fichero, y obtenemos el fichero flag.bin.

Al hacer ```$ file flag.bin```, nos indica que el contenido del fichero es texto ASCII, al hacer cat del fichero podemos ver que siempre se repite la misma cadena de caracteres: 
> ZmxhZ3s5ODMyMDE2NmVhN2I0N2M4NzFlNzY1NTU1M2QzNGU4OX0=

Por el símbolo del final, probamos a hacer decode from base64
```
$ echo "ZmxhZ3s5ODMyMDE2NmVhN2I0N2M4NzFlNzY1NTU1M2QzNGU4OX0=" | base64 -d
```

Obtenemos la flag.
**flag{98320166ea7b47c871e7655553d34e89}**
